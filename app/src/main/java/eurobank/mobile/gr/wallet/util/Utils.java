/*
 *  Copyright (c) 2018, MasterCard International Incorporated and/or its
 *  affiliates. All rights reserved.
 *
 *  The contents of this file may only be used subject to the MasterCard
 *  Mobile Payment SDK for MCBP and/or MasterCard Mobile MPP UI SDK
 *  Materials License.
 *
 *  Please refer to the file LICENSE.TXT for full details.
 *
 *  TO THE EXTENT PERMITTED BY LAW, THE SOFTWARE IS PROVIDED "AS IS", WITHOUT
 *  WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NON INFRINGEMENT. TO THE EXTENT PERMITTED BY LAW, IN NO EVENT SHALL
 *  MASTERCARD OR ITS AFFILIATES BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 */

package eurobank.mobile.gr.wallet.util;

import android.Manifest;
import android.app.Activity;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.mastercard.mpsdk.walletusabilitylayer.api.Wul;
import com.mastercard.mpsdk.walletusabilitylayer.log.WLog;
import com.mastercard.mpsdk.walletusabilitylayer.util.WalletUtils;

import java.util.Random;

import eurobank.mobile.gr.wallet.BuildConfig;
import eurobank.mobile.gr.wallet.Constants;
import eurobank.mobile.gr.wallet.R;

public class Utils {
    public static void popToast(final Context con, final String message) {
        Toast.makeText(con, message, Toast.LENGTH_SHORT).show();
    }

    public static AlertDialog getDialog(final Context con, final String message) {
        AlertDialog.Builder d = new AlertDialog.Builder(con);
        d.setMessage(message).setCancelable(false).setTitle(null);
        return d.create();
    }

    public static AlertDialog getErrorDialog(final Context con, final String title, final String
            message) {
        AlertDialog.Builder d = new AlertDialog.Builder(con);
        d.setMessage(message).setCancelable(true).setTitle(title);
        d.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        return d.create();
    }

    public static AlertDialog.Builder getModalDialog(final Context con, final String message) {
        AlertDialog.Builder d = new AlertDialog.Builder(con);
        d.setCancelable(false);
        d.setMessage(message).setCancelable(true).setTitle(null);
        d.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        return d;
    }

    public static void showNotification(final Context con, final String title, final String
            message) {

        Notification.Builder builder = new Notification.Builder(con)
                .setSmallIcon(R.drawable.mastercard_logo)
                .setLargeIcon(BitmapFactory.decodeResource(con.getResources(), R.drawable
                        .mastercard_logo))
                .setContentTitle(title)
                .setContentText(message);

        Notification notification = builder.build();

        NotificationManager notificationManager = (NotificationManager) con.getSystemService
                (Context.NOTIFICATION_SERVICE);
        notificationManager.notify(new Random().nextInt(), notification);
    }

    public static void showKeyboard(final Context con, final View view) {
        InputMethodManager i = (InputMethodManager) con.getSystemService(Context
                .INPUT_METHOD_SERVICE);
        i.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    public static void hideKeyboard(final Context con, final View view) {
        InputMethodManager i = (InputMethodManager) con.getSystemService(Context
                .INPUT_METHOD_SERVICE);
        i.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void resetAppAndRelaunch(final Activity act) {
        WLog.d("Utils", "**** resetAppAndRelaunch");

        // reset PAS/Issuer URLs
        setPasUrl(act, null);
        setIssuerIp(act, null);
        setIssuerPort(act, 0);

        // reset WUL
        Wul.reset();

        // re-initialize WUL
        Wul.getWulApplication().init();

        // restart app
        Intent i = act.getPackageManager().getLaunchIntentForPackage(act.getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        act.startActivity(i);
    }

    public static void setPasUrl(final Context context, final String url) {
        SharedPreferences prefs = context.getSharedPreferences(Constants.PREFS_DB, Context
                .MODE_PRIVATE);
        prefs.edit().putString(Constants.PREFS_PAS_URL, url).apply();
    }

    public static String getPasUrl(final Context context) {
        SharedPreferences prefs = context.getSharedPreferences(Constants.PREFS_DB, Context
                .MODE_PRIVATE);
        return prefs.getString(Constants.PREFS_PAS_URL, BuildConfig.PAS_URL);
    }

    public static void setIssuerIp(final Context context, final String url) {
        SharedPreferences prefs = context.getSharedPreferences(Constants.PREFS_DB, Context
                .MODE_PRIVATE);
        prefs.edit().putString(Constants.PREFS_ISSUER_IP, url).apply();
    }

    public static void setIssuerPort(final Context context, final int port) {
        SharedPreferences prefs = context.getSharedPreferences(Constants.PREFS_DB, Context
                .MODE_PRIVATE);
        prefs.edit().putInt(Constants.PREFS_ISSUER_PORT, port).apply();
    }

    public static String getIssuerIp(final Context context) {
        SharedPreferences prefs = context.getSharedPreferences(Constants.PREFS_DB, Context
                .MODE_PRIVATE);
        return prefs.getString(Constants.PREFS_ISSUER_IP, BuildConfig.ISSUER_IP);
    }

    public static int getIssuerPort(final Context context) {
        SharedPreferences prefs = context.getSharedPreferences(Constants.PREFS_DB, Context
                .MODE_PRIVATE);
        return prefs.getInt(Constants.PREFS_ISSUER_PORT, BuildConfig.ISSUER_PORT);
    }

    public static boolean hasFingerprintPermission(final Context con) {
        try {
            if (ActivityCompat.checkSelfPermission(con, Manifest.permission.USE_FINGERPRINT) !=
                    PackageManager.PERMISSION_GRANTED) {
                WLog.d("Utils", "Fingerprint authentication permission: not enabled");
                return false;
            }
            WLog.d("Utils", "Fingerprint authentication permission: enabled");
            return true;
        } catch (Exception e) {
            WLog.e("Utils", "Failed to determine fingerprint permission", e);
        }
        return false;
    }

    public static boolean hasFingerprintHardware(final Context con) {
        try {
            FingerprintManagerCompat fm = FingerprintManagerCompat.from(con);
            return fm.isHardwareDetected();
        } catch (NullPointerException | SecurityException e) {
            WLog.e("Utils", "Failed to determine fingerprint hardware", e);
        }
        return false;
    }

    public static boolean hasFingerprintsEnrolledOnDevice(final Context con) {
        try {
            FingerprintManagerCompat fm = FingerprintManagerCompat.from(con);
            return fm.hasEnrolledFingerprints();
        } catch (NullPointerException | SecurityException e) {
            WLog.e("Utils", "Failed to determine fingerprint hardware", e);
        }
        return false;
    }

    public static boolean isScreenLocked(final Context context) {
        try {
            final KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService
                    (Context.KEYGUARD_SERVICE);
            return keyguardManager.inKeyguardRestrictedInputMode();
        } catch (Exception e) {
            WLog.e("Utils", "Failed to determine lockscreen state", e);
        }
        return false;
    }

    public static boolean isLockscreenEnabled(final Context con) {
        try {
            KeyguardManager km = (KeyguardManager) con.getSystemService(Context.KEYGUARD_SERVICE);
            return km.isKeyguardSecure();
        } catch (Exception e) {
            WLog.e("Utils", "Failed to determine lockscreen secure state", e);
        }
        return false;
    }

    public static String randomizePan() {
        Random r = new Random();
        StringBuilder sb = new StringBuilder("5");
        for (int n = 0; n < 14; n++) {
            sb.append(r.nextInt(10));
        }
        for (int n = 0; n < 10; n++) {
            sb.append(n);
            if (WalletUtils.validateLuhn(sb.toString())) {
                return sb.toString();
            }
            sb.setLength(15);
        }
        return "";
    }

    public static void dumpIntent(Intent i) {
        Bundle b = i.getExtras();
        if (b != null) {
            for (String key : b.keySet()) {
                Object value = b.get(key);
                if (value != null) {
                    WLog.d("Utils", String.format("%s=%s [%s]", key, value.toString(), value.getClass().getName()));
                }
            }
        }
    }
}
