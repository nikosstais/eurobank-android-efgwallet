/*
 *  Copyright (c) 2018, MasterCard International Incorporated and/or its
 *  affiliates. All rights reserved.
 *
 *  The contents of this file may only be used subject to the MasterCard
 *  Mobile Payment SDK for MCBP and/or MasterCard Mobile MPP UI SDK
 *  Materials License.
 *
 *  Please refer to the file LICENSE.TXT for full details.
 *
 *  TO THE EXTENT PERMITTED BY LAW, THE SOFTWARE IS PROVIDED "AS IS", WITHOUT
 *  WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NON INFRINGEMENT. TO THE EXTENT PERMITTED BY LAW, IN NO EVENT SHALL
 *  MASTERCARD OR ITS AFFILIATES BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 */

package eurobank.mobile.gr.wallet;

public class Constants {
    public static final String PREFS_DB = "sample_mpa_wul";
    public static final String PREFS_PAS_URL = "pas_url";
    public static final String PREFS_ISSUER_IP = "issuer_ip";
    public static final String PREFS_ISSUER_PORT = "issuer_port";

    public static final int RESULT_CARD_ADDED = 1;
    public static final int RESULT_PIN_ENTERED = 2;
    public static final int RESULT_FINGERPRINT_PERMISSION_REFUSED = 3;
    public static final int RESULT_PIN_DSRP_AUTH_SUCCESS = 4;
    public static final int RESULT_AOT_COUNTER_EXPIRED = 5;
    public static final int RESULT_CANCELLED = 6;
}
