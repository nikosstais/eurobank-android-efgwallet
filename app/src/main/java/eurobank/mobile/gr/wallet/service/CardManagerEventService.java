/*
 *  Copyright (c) 2018, MasterCard International Incorporated and/or its
 *  affiliates. All rights reserved.
 *
 *  The contents of this file may only be used subject to the MasterCard
 *  Mobile Payment SDK for MCBP and/or MasterCard Mobile MPP UI SDK
 *  Materials License.
 *
 *  Please refer to the file LICENSE.TXT for full details.
 *
 *  TO THE EXTENT PERMITTED BY LAW, THE SOFTWARE IS PROVIDED "AS IS", WITHOUT
 *  WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NON INFRINGEMENT. TO THE EXTENT PERMITTED BY LAW, IN NO EVENT SHALL
 *  MASTERCARD OR ITS AFFILIATES BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 */

package eurobank.mobile.gr.wallet.service;

import com.mastercard.mpsdk.componentinterface.PaymentContext;
import com.mastercard.mpsdk.walletusabilitylayer.api.Wul;
import com.mastercard.mpsdk.walletusabilitylayer.api.WulCardManager;
import com.mastercard.mpsdk.walletusabilitylayer.log.WLog;
import com.mastercard.mpsdk.walletusabilitylayer.model.WulCard;
import com.mastercard.mpsdk.walletusabilitylayer.receiver.WalletCardManagerEventReceiver;
import com.mastercard.mpsdk.walletusabilitylayer.service.WalletService;

import eurobank.mobile.gr.wallet.util.Utils;

import static com.mastercard.mpsdk.walletusabilitylayer.log.WLog.d;

/**
 * Service listening for card management events (provision, PIN change etc).
 * We are a PIN-based wallet, so we listen for provision events and wallet PIN events.
 */
public class CardManagerEventService extends WalletService {
    /**
     * Receiver for card manager events
     */
    private class LocalWalletCardManagerEventReceiver extends WalletCardManagerEventReceiver {
        @Override
        public boolean onCardProvisionCompleted(final WulCard card) {

            // if we have only one card, make sure it is the default
            WulCardManager cardManager = Wul.getCardManager();
            if (cardManager.getCardCount() == 1) {
                cardManager.setCardAsDefault(card, PaymentContext.CONTACTLESS);
                cardManager.setCardAsDefault(card, PaymentContext.DSRP);
            }
            cardManager.removeDigitizingCard();

            String title = "New card received";
            String message = "Your card ***-" + card.getDisplayablePanDigits() + " is now ready " +
                    "to use.";
            Utils.showNotification(CardManagerEventService.this, title, message);
            return true;
        }

        @Override
        public boolean onCardProvisionFailure(String errorCode, String errorMessage, Exception e) {
            WLog.d(this, "**** [CardManagerEventService] onCardProvisionFailure code=" +
                    errorCode + " message=" + errorMessage);
            Wul.getCardManager().removeDigitizingCard();
            Utils.showNotification(CardManagerEventService.this, "Card provision failed",
                    errorMessage + " [code=" + errorCode + "]");
            return true;
        }

        @Override
        public boolean onSetWalletPinCompleted() {
            String title = "Wallet PIN set";
            String message = "The PIN for your wallet has been set.";
            Utils.showNotification(CardManagerEventService.this, title, message);
            return true;
        }

        @Override
        public boolean onWalletPinChangeCompleted() {
            String title = "Wallet PIN has been changed";
            String message = "The PIN for your wallet has been changed.";
            Utils.showNotification(CardManagerEventService.this, title, message);
            return true;
        }
    }

    private final LocalWalletCardManagerEventReceiver cardManagerEventReceiver = new LocalWalletCardManagerEventReceiver();

    @Override
    public void onCreate() {
        d(this, "**** onCreate");
        cardManagerEventReceiver.register(this);
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        d(this, "**** onDestroy");
        cardManagerEventReceiver.unregister(this);
        super.onDestroy();
    }
}
