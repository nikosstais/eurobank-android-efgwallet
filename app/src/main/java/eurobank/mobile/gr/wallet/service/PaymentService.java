/*
 *  Copyright (c) 2018, MasterCard International Incorporated and/or its
 *  affiliates. All rights reserved.
 *
 *  The contents of this file may only be used subject to the MasterCard
 *  Mobile Payment SDK for MCBP and/or MasterCard Mobile MPP UI SDK
 *  Materials License.
 *
 *  Please refer to the file LICENSE.TXT for full details.
 *
 *  TO THE EXTENT PERMITTED BY LAW, THE SOFTWARE IS PROVIDED "AS IS", WITHOUT
 *  WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NON INFRINGEMENT. TO THE EXTENT PERMITTED BY LAW, IN NO EVENT SHALL
 *  MASTERCARD OR ITS AFFILIATES BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 */

package eurobank.mobile.gr.wallet.service;


//import android.content.Intent;

//import com.mastercard.mcbp.sample.wul.activity.TransactionActivity;
import com.mastercard.mpsdk.walletusabilitylayer.receiver.WalletContactlessTransactionEventReceiver;
import com.mastercard.mpsdk.walletusabilitylayer.service.WalletService;

//import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static com.mastercard.mpsdk.walletusabilitylayer.log.WLog.d;

/**
 * Service listening for contactless payment events.
 * to prompt user action.
 */
public class PaymentService extends WalletService {
    private class LocalWalletContactlessTransactionEventReceiver extends
            WalletContactlessTransactionEventReceiver {
        @Override
        public void onContactlessTransactionStarted() {
            d(this, "**** [PaymentService] contactlessTransactionStarted");
            // launch the activity that will be handling this transaction
//            Intent i = new Intent(PaymentService.this, TransactionActivity.class);
//            i.setFlags(FLAG_ACTIVITY_NEW_TASK);
//            startActivity(i);
        }
    }

    private final LocalWalletContactlessTransactionEventReceiver
            contactlessTransactionEventReceiver = new
            LocalWalletContactlessTransactionEventReceiver();

    @Override
    public void onCreate() {
        d(this, "**** onCreate");
        contactlessTransactionEventReceiver.register(this);
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        d(this, "**** onDestroy");
        contactlessTransactionEventReceiver.unregister(this);
        super.onDestroy();
    }

}
