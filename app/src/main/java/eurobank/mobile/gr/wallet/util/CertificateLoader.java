/*
 *  Copyright (c) 2018, MasterCard International Incorporated and/or its
 *  affiliates. All rights reserved.
 *
 *  The contents of this file may only be used subject to the MasterCard
 *  Mobile Payment SDK for MCBP and/or MasterCard Mobile MPP UI SDK
 *  Materials License.
 *
 *  Please refer to the file LICENSE.TXT for full details.
 *
 *  TO THE EXTENT PERMITTED BY LAW, THE SOFTWARE IS PROVIDED "AS IS", WITHOUT
 *  WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NON INFRINGEMENT. TO THE EXTENT PERMITTED BY LAW, IN NO EVENT SHALL
 *  MASTERCARD OR ITS AFFILIATES BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 */

package eurobank.mobile.gr.wallet.util;


import android.content.Context;
import android.os.AsyncTask;

import com.mastercard.mpsdk.walletusabilitylayer.exception.HttpCommunicationException;
import com.mastercard.mpsdk.walletusabilitylayer.http.SimpleHttpConnection;
import com.mastercard.mpsdk.walletusabilitylayer.log.WLog;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class CertificateLoader extends AsyncTask<Void, Void, byte[]> {

    private Context mContext;
    private String mCertificateLocation;
    private Callback mCallback;

    public interface Callback {
        void certificateLoaded(final byte[] certificateBytes);
    }

    public CertificateLoader(final Context context, final String certificateLocation, final
    Callback callback) {
        mContext = context;
        mCertificateLocation = certificateLocation;
        mCallback = callback;
    }

    @Override
    protected byte[] doInBackground(Void... params) {
        byte[] responseData = null;
        try {
            if (mCertificateLocation.startsWith("http")) {
                // download the certificate from the CMS
                final SimpleHttpConnection connection = new SimpleHttpConnection
                        (mCertificateLocation);
                responseData = connection.execute();
            } else {
                // load the certificate from assets
                InputStream in = mContext.getAssets().open(mCertificateLocation);
                BufferedInputStream bis = new BufferedInputStream(in);
                ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                int b;
                while ((b = bis.read()) != - 1) {
                    buffer.write(b);
                }
                bis.close();
                buffer.close();
                responseData = buffer.toByteArray();
            }
        } catch (IOException | HttpCommunicationException e) {
            WLog.e(this, "Failed to load certificate bytes", e);
        }
        return responseData;
    }

    @Override
    protected void onPostExecute(byte[] certificateBytes) {
        mContext = null;
        if (mCallback != null) {
            mCallback.certificateLoaded(certificateBytes);
        }
    }

}
